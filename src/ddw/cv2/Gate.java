/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ddw.cv2;

import gate.util.compilers.eclipse.jdt.internal.compiler.ast.ThisReference;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Degee
 */
public class Gate {

    private static String parseUrl(String url) {
        if (url.contains("http://")) {
            return url;
        }
        return "http://" + url;
    }

    private static String getContentFromUrl(String url) throws MalformedURLException, IOException {
        URL tmp = new URL(parseUrl(url));
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        tmp.openStream()
                )
        );

        String inputLine;
        String ret = "";
        while ((inputLine = in.readLine()) != null) {
            ret += inputLine;
        }
        in.close();

        return ret;
    }

    public static void main(String[] args) {
        org.apache.log4j.BasicConfigurator.configure();
        org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);

        String url = "fit.cvut.cz";
        if (args.length > 0) {
            url = args[0];
        }
        
        try {
            GateClient client = new GateClient();
            client.setSource(new URL(parseUrl(url)));
            
            System.out.println();
            System.out.println();
            System.out.println("Trying get all emails from: " + parseUrl(url));
            System.out.println();
            
            client.run();
        } catch (MalformedURLException ex) {
            Logger.getLogger(Gate.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
